/*
 * ALSA SoC pcm1690 driver
 *
 * Author:      Josh Elliott, <jelliott@ti.com>
 * Copyright:   Copyright:   (C) 2014  Texas Instruments
 *
 * Based on sound/soc/codecs/spdif_transmitter.c by Steve Chen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/slab.h>
#include <sound/soc.h>
#include <sound/pcm.h>
#include <sound/initval.h>
#include <linux/of.h>

#define DRV_NAME "pcm1690"

#define RATES		SNDRV_PCM_RATE_8000_96000
#define FORMATS		(SNDRV_PCM_FMTBIT_S16_LE | SNDRV_PCM_FMTBIT_S20_3LE | \
			 SNDRV_PCM_FMTBIT_S24_3LE | SNDRV_PCM_FMTBIT_S32_LE)

static struct snd_soc_codec_driver soc_codec_pcm1690 = {

};

static struct snd_soc_dai_driver pcm1690_dai = {
	.name		= "pcm1690-hifi",
	.playback 	= {
		.stream_name	= "Playback",
		.channels_min	= 1,
		.channels_max	= 2,
		.rates		= RATES,
		.formats	= FORMATS,
	},
};

static int pcm1690_probe(struct platform_device *pdev)
{
	printk("###pcm1690 probe...\n");


	return snd_soc_register_codec(&pdev->dev, &soc_codec_pcm1690,
			&pcm1690_dai, 1);
}

static int pcm1690_remove(struct platform_device *pdev)
{
	snd_soc_unregister_codec(&pdev->dev);
	return 0;
}

/*	set in davinci-evm
#ifdef CONFIG_OF
static const struct of_device_id pcm1690_dt_ids[] = {
	{ .compatible = "ti,pcm1690", },
	{ }
};
MODULE_DEVICE_TABLE(of, pcm1690_dt_ids);
#endif
*/

static struct platform_driver pcm1690_driver = {
	.probe		= pcm1690_probe,
	.remove		= pcm1690_remove,
	.driver		= {
		.name	= DRV_NAME,
		.owner	= THIS_MODULE,
		//.of_match_table = of_match_ptr(pcm1690_dt_ids),
	},
};

module_platform_driver(pcm1690_driver);

/* machine i2c codec control layer */
static struct i2c_driver pcm1690_i2c_driver = {
	.driver = {
		.name = "pcm1690-codec",
		.owner = THIS_MODULE,
		//.of_match_table = of_match_ptr(pcm1690_of_match),
	},
	//.probe	= aic3x_i2c_probe,
	//.remove	= aic3x_i2c_remove,
	//.id_table	= aic3x_i2c_id,
};

module_i2c_driver(pcm1690_i2c_driver);

MODULE_AUTHOR("Josh Elliott <jelliott@ti.com>");
MODULE_DESCRIPTION("PCM1690 codec driver");
MODULE_LICENSE("GPL");
