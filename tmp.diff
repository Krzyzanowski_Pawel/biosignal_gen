--- ../../../kernel-3.13/kernel/kernel/sound/soc/davinci/davinci-evm.c	2016-01-11 10:49:24.489731000 +0100
+++ originals/sound/soc/davinci/davinci-evm.c	2016-01-11 10:40:47.000000000 +0100
@@ -30,9 +30,8 @@
 #include "davinci-i2s.h"
 #include "davinci-mcasp.h"
 
-struct snd_soc_card_drvdata_davinci {
-	unsigned sysclk;
-};
+#include <linux/of_gpio.h>
+
 
 #define AUDIO_FORMAT (SND_SOC_DAIFMT_DSP_B | \
 		SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_IB_NF)
@@ -44,9 +43,36 @@
 	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
 	struct snd_soc_codec *codec = rtd->codec;
 	struct snd_soc_card *soc_card = codec->card;
+	struct device_node *np = soc_card->dev->of_node;
 	int ret = 0;
-	unsigned sysclk = ((struct snd_soc_card_drvdata_davinci *)
-			   snd_soc_card_get_drvdata(soc_card))->sysclk;
+	unsigned sysclk;
+
+	if (np) {
+		ret = of_property_read_u32(np, "ti,codec-clock-rate", &sysclk);
+		if (ret < 0)
+			return ret;
+	} else {
+		/* ASP1 on DM355 EVM is clocked by an external oscillator */
+		if (machine_is_davinci_dm355_evm() ||
+			machine_is_davinci_dm6467_evm() ||
+			machine_is_davinci_dm365_evm())
+			sysclk = 27000000;
+
+		/*
+		 * ASP0 in DM6446 EVM is clocked by U55, as configured by
+		 * board-dm644x-evm.c using GPIOs from U18.  There are six
+		 * options; here we "know" we use a 48 KHz sample rate.
+		 */
+		else if (machine_is_davinci_evm())
+			sysclk = 12288000;
+
+		else if (machine_is_davinci_da830_evm() ||
+					machine_is_davinci_da850_evm())
+			sysclk = 24576000;
+
+		else
+			return -EINVAL;
+	}
 
 	/* set codec DAI configuration */
 	ret = snd_soc_dai_set_fmt(codec_dai, AUDIO_FORMAT);
@@ -108,9 +134,9 @@
 	{"Line Out", NULL, "RLOUT"},
 
 	/* Mic connected to (MIC3L | MIC3R) */
-	{"MIC3L", NULL, "Mic Bias"},
-	{"MIC3R", NULL, "Mic Bias"},
-	{"Mic Bias", NULL, "Mic Jack"},
+	{"MIC3L", NULL, "Mic Bias 2V"},
+	{"MIC3R", NULL, "Mic Bias 2V"},
+	{"Mic Bias 2V", NULL, "Mic Jack"},
 
 	/* Line In connected to (LINE1L | LINE2L), (LINE1R | LINE2R) */
 	{"LINE1L", NULL, "Line In"},
@@ -119,10 +145,37 @@
 	{"LINE2R", NULL, "Line In"},
 };
 
+/* Logic for a tda998x as connected on a davinci-evm */
+static int evm_tda998x_init(struct snd_soc_pcm_runtime *rtd)
+{
+	struct snd_soc_dai *codec_dai = rtd->codec_dai;
+	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
+	int ret;
+
+	ret = snd_soc_dai_set_clkdiv(cpu_dai, 0, 1);
+	if (ret < 0)
+		return ret;
+
+	ret = snd_soc_dai_set_clkdiv(cpu_dai, 1, 8);
+	if (ret < 0)
+		return ret;
+
+	ret = snd_soc_dai_set_sysclk(cpu_dai, 0, 0, SND_SOC_CLOCK_IN);
+	if (ret < 0)
+		return ret;
+
+	ret = snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_CBS_CFS | SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_IB_NF);
+	if (ret < 0)
+		return ret;
+
+	return 0;
+}
+
 /* Logic for a aic3x as connected on a davinci-evm */
 static int evm_aic3x_init(struct snd_soc_pcm_runtime *rtd)
 {
 	struct snd_soc_codec *codec = rtd->codec;
+	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
 	struct snd_soc_dapm_context *dapm = &codec->dapm;
 	struct device_node *np = codec->card->dev->of_node;
 	int ret;
@@ -141,6 +194,11 @@
 		snd_soc_dapm_add_routes(dapm, audio_map, ARRAY_SIZE(audio_map));
 	}
 
+	/* Divide McASP MCLK by 2 to provide 12MHz to codec */
+	ret = snd_soc_dai_set_clkdiv(cpu_dai, 0, 2);
+	if (ret < 0)
+		return ret;
+
 	/* not connected */
 	snd_soc_dapm_disable_pin(dapm, "MONO_LOUT");
 	snd_soc_dapm_disable_pin(dapm, "HPLCOM");
@@ -243,65 +301,35 @@
 };
 
 /* davinci dm6446 evm audio machine driver */
-/*
- * ASP0 in DM6446 EVM is clocked by U55, as configured by
- * board-dm644x-evm.c using GPIOs from U18.  There are six
- * options; here we "know" we use a 48 KHz sample rate.
- */
-static struct snd_soc_card_drvdata_davinci dm6446_snd_soc_card_drvdata = {
-	.sysclk = 12288000,
-};
-
 static struct snd_soc_card dm6446_snd_soc_card_evm = {
 	.name = "DaVinci DM6446 EVM",
 	.owner = THIS_MODULE,
 	.dai_link = &dm6446_evm_dai,
 	.num_links = 1,
-	.drvdata = &dm6446_snd_soc_card_drvdata,
 };
 
 /* davinci dm355 evm audio machine driver */
-/* ASP1 on DM355 EVM is clocked by an external oscillator */
-static struct snd_soc_card_drvdata_davinci dm355_snd_soc_card_drvdata = {
-	.sysclk = 27000000,
-};
-
 static struct snd_soc_card dm355_snd_soc_card_evm = {
 	.name = "DaVinci DM355 EVM",
 	.owner = THIS_MODULE,
 	.dai_link = &dm355_evm_dai,
 	.num_links = 1,
-	.drvdata = &dm355_snd_soc_card_drvdata,
 };
 
 /* davinci dm365 evm audio machine driver */
-static struct snd_soc_card_drvdata_davinci dm365_snd_soc_card_drvdata = {
-	.sysclk = 27000000,
-};
-
 static struct snd_soc_card dm365_snd_soc_card_evm = {
 	.name = "DaVinci DM365 EVM",
 	.owner = THIS_MODULE,
 	.dai_link = &dm365_evm_dai,
 	.num_links = 1,
-	.drvdata = &dm365_snd_soc_card_drvdata,
 };
 
 /* davinci dm6467 evm audio machine driver */
-static struct snd_soc_card_drvdata_davinci dm6467_snd_soc_card_drvdata = {
-	.sysclk = 27000000,
-};
-
 static struct snd_soc_card dm6467_snd_soc_card_evm = {
 	.name = "DaVinci DM6467 EVM",
 	.owner = THIS_MODULE,
 	.dai_link = dm6467_evm_dai,
 	.num_links = ARRAY_SIZE(dm6467_evm_dai),
-	.drvdata = &dm6467_snd_soc_card_drvdata,
-};
-
-static struct snd_soc_card_drvdata_davinci da830_snd_soc_card_drvdata = {
-	.sysclk = 24576000,
 };
 
 static struct snd_soc_card da830_snd_soc_card = {
@@ -309,11 +337,6 @@
 	.owner = THIS_MODULE,
 	.dai_link = &da830_evm_dai,
 	.num_links = 1,
-	.drvdata = &da830_snd_soc_card_drvdata,
-};
-
-static struct snd_soc_card_drvdata_davinci da850_snd_soc_card_drvdata = {
-	.sysclk = 24576000,
 };
 
 static struct snd_soc_card da850_snd_soc_card = {
@@ -321,35 +344,48 @@
 	.owner = THIS_MODULE,
 	.dai_link = &da850_evm_dai,
 	.num_links = 1,
-	.drvdata = &da850_snd_soc_card_drvdata,
 };
 
+
 #if defined(CONFIG_OF)
 
-/*
- * The struct is used as place holder. It will be completely
- * filled with data from dt node.
- */
-static struct snd_soc_dai_link evm_dai_tlv320aic3x = {
-	.name		= "TLV320AIC3X",
-	.stream_name	= "AIC3X",
-	.codec_dai_name	= "tlv320aic3x-hifi",
-	.ops            = &evm_ops,
-	.init           = evm_aic3x_init,
+enum {
+	MACHINE_VERSION_1 = 0,	/* DM365 with Voice Codec */
+	MACHINE_VERSION_2,	/* DM365/DA8xx/OMAPL1x/AM33xx */
+	MACHINE_VERSION_3,	/* AM33xx BeagleBone Black */
 };
 
 static const struct of_device_id davinci_evm_dt_ids[] = {
 	{
+		.compatible = "ti,dm365-voice-codec-audio",
+		.data = (void *)MACHINE_VERSION_1,
+	},
+	{
 		.compatible = "ti,da830-evm-audio",
-		.data = (void *) &evm_dai_tlv320aic3x,
+		.data = (void *)MACHINE_VERSION_2,
+	},
+	{
+		.compatible = "ti,am33xx-beaglebone-black",
+		.data = (void *)MACHINE_VERSION_3,
 	},
 	{ /* sentinel */ }
 };
-MODULE_DEVICE_TABLE(of, davinci_evm_dt_ids);
+MODULE_DEVICE_TABLE(of, davinci_mcasp_dt_ids);
+
+/*
+ * This struct is just used as place holder. It will be filled with
+ * data from dt node
+ */
+static struct snd_soc_dai_link evm_dai = {
+	.name		= "TLV320AIC3X",
+	.stream_name	= "AIC3X",
+	.codec_dai_name	= "tlv320aic3x-hifi",
+};
 
 /* davinci evm audio machine driver */
 static struct snd_soc_card evm_soc_card = {
 	.owner = THIS_MODULE,
+	.dai_link = &evm_dai,
 	.num_links = 1,
 };
 
@@ -358,38 +394,63 @@
 	struct device_node *np = pdev->dev.of_node;
 	const struct of_device_id *match =
 		of_match_device(of_match_ptr(davinci_evm_dt_ids), &pdev->dev);
-	struct snd_soc_dai_link *dai = (struct snd_soc_dai_link *) match->data;
-	struct snd_soc_card_drvdata_davinci *drvdata = NULL;
+	u32 machine_ver, clk_gpio;
 	int ret = 0;
 
-	evm_soc_card.dai_link = dai;
+	machine_ver = (u32)match->data;
+	switch (machine_ver) {
+	case MACHINE_VERSION_1:
+		evm_dai.name		= "Voice Codec - CQ93VC";
+		evm_dai.stream_name	= "CQ93";
+		evm_dai.codec_dai_name	= "cq93vc-hifi";
+		break;
+	case MACHINE_VERSION_2:
+		evm_dai.ops = &evm_ops;
+		evm_dai.init = evm_aic3x_init;
+		break;
+	case MACHINE_VERSION_3:
+		evm_dai.name		= "NXP TDA HDMI Chip";
+		evm_dai.stream_name	= "HDMI";
+		evm_dai.codec_dai_name	= "nxp-hdmi-hifi";
+		evm_dai.init = evm_tda998x_init;
+
+		/*
+		 * Move GPIO handling out of the probe, if probe gets
+		 * deferred, the gpio will have been claimed on previous
+		 * probe and will fail on the second and susequent probes
+		 */
+		clk_gpio = of_get_named_gpio(np, "mcasp_clock_enable", 0);
+		if (clk_gpio < 0) {
+		  dev_err(&pdev->dev, "failed to find mcasp_clock enable GPIO!\n");
+		  return -EINVAL;
+		}
+		ret = gpio_request_one(clk_gpio, GPIOF_OUT_INIT_HIGH,
+				       "McASP Clock Enable Pin");
+		if (ret < 0) {
+		  dev_err(&pdev->dev, "Failed to claim McASP Clock Enable pin\n");
+		  return -EINVAL;
+		}
+		gpio_set_value(clk_gpio, 1);
+		break;
+	}
 
-	dai->codec_of_node = of_parse_phandle(np, "ti,audio-codec", 0);
-	if (!dai->codec_of_node)
+	evm_dai.codec_of_node = of_parse_phandle(np, "ti,audio-codec", 0);
+	if (!evm_dai.codec_of_node)
 		return -EINVAL;
 
-	dai->cpu_of_node = of_parse_phandle(np, "ti,mcasp-controller", 0);
-	if (!dai->cpu_of_node)
+	evm_dai.cpu_of_node = of_parse_phandle(np,
+						"ti,mcasp-controller", 0);
+	if (!evm_dai.cpu_of_node)
 		return -EINVAL;
 
-	dai->platform_of_node = dai->cpu_of_node;
+	evm_dai.platform_of_node = evm_dai.cpu_of_node;
 
 	evm_soc_card.dev = &pdev->dev;
 	ret = snd_soc_of_parse_card_name(&evm_soc_card, "ti,model");
 	if (ret)
 		return ret;
 
-	drvdata = devm_kzalloc(&pdev->dev, sizeof(*drvdata), GFP_KERNEL);
-	if (!drvdata)
-		return -ENOMEM;
-
-	ret = of_property_read_u32(np, "ti,codec-clock-rate", &drvdata->sysclk);
-	if (ret < 0)
-		return -EINVAL;
-
-	snd_soc_card_set_drvdata(&evm_soc_card, drvdata);
-	ret = devm_snd_soc_register_card(&pdev->dev, &evm_soc_card);
-
+	ret = snd_soc_register_card(&evm_soc_card);
 	if (ret)
 		dev_err(&pdev->dev, "snd_soc_register_card failed (%d)\n", ret);
 
@@ -424,11 +485,11 @@
 	int index;
 	int ret;
 
+#if defined(CONFIG_OF)
 	/*
 	 * If dtb is there, the devices will be created dynamically.
 	 * Only register platfrom driver structure.
 	 */
-#if defined(CONFIG_OF)
 	if (of_have_populated_dt())
 		return platform_driver_register(&davinci_evm_driver);
 #endif
